# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Known Bugs


## [Unreleased]

## [0.0.1] - 2022-03-09

### Fixed

### Added
- gitignore

### Changed
- Readme

### Removed
- 

[Unreleased]: Link to comparison between develop and master branch

[v0.0.1]: Link to version comparison in Gitlab (commit, tag...)
