# Docker folder
This folder contains the docker-related files. The project is intended to be run in docker containers, using compose as orchestrator. Each module sub-folder contains a Dockerfile in charge of building the corresponding docker image. Dockerfile invokes them through independent compose services.

## License
© Open Source Licence - AGRICORE