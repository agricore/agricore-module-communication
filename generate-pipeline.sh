#!/bin/sh

directories=$(find image/ -type f -name "Dockerfile")

for df in $directories
do
  # Remove prefix image/ from path 
  tag=${df#image/}
  
  # Remove /Dockerfile from the end of the path
  tag=${tag%/Dockerfile}
  
  # Replace / with -
  tag=${tag//\//-}
  cat <<EOF
build-$tag:  
  stage: build
  image: docker:stable
  variables:
    IMAGE_TAG: registry.gitlab.com/agricore/agricore-module-communication/$tag
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""    
  services:
    - docker:dind
  script:
    - echo "\$DOCKER_REGISTRY_PASSWORD" | docker login registry.gitlab.com -u docker-registry-token --password-stdin;
    - docker build -t \$IMAGE_TAG -f $df $(dirname $df)
    - docker push \$IMAGE_TAG
  only:
    changes:
      - $(dirname $df)/**/*
EOF
done
