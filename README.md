# AGRICORE Module Communication
Project focused on preparing a demo of the intended intermodule communication as discussed in deliverable D6.1. 

Contents generated here will be part of deliverable D6.2

## Installation
As the project is based on Docker technologies, no local installation of dependencies is needed, except for the Docker-related tools (Docker and docker-compose).

## Usage
Navigate to the docker folder and run 

`docker-compose up -d`

To terminate the execution run

`docker-compose down`

## Production
The production version of the compose uses pre-deployed images from the Gitlab docker registry. These images are compiled automatically using a CI-CD pipeline everytime a version is merged into master. 

To run the production version, the override compose file `docker-compose.prod.yml` shall be used together with the original one:

`docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`

Note: The credentials to the registry can be found in Bitwarden, under the IT & DIGITAL APPS collection with the entry name: "Agricore module communication deploy token".

## Contributing
Merge requests for enhancement proposals are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
© Open Source Licence - AGRICORE
