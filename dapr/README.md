# Dapr folder
This folder contains the Dapr files. Dapr is used as inter-module communication engine. Each module has its own Dapr sidecar for the communication with the rest of the architecture. In this folder, Dapr is configured and the different components are declared under the `components` folder.

## License
© Open Source Licence - AGRICORE