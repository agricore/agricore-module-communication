# ABM - Agent-Based Model
Python app implementing AGRICORE's Agent-Based Model Module. This module will simulate the evolution of the ABM population according to the AGRICORE agent dynamics previously discussed in this section. The implementation of this module will be fully object-oriented and the agents will be instantiated according to the synthetic population already
generated in the preceding modules. On runtime, this module will connect to a mathematical solver in order to perform the iterations needed to simulate the evolution of the agents and it will also manage the interactions required with the external modules (land, markets, environment, etc).

## License
© Open Source Licence - AGRICORE