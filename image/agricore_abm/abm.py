# General imports
from enum import Enum
import json
from time import sleep

from cloudevents.sdk.event import v1

# Dapr and gRPC related imports
from dapr.clients import DaprClient
from dapr.ext.grpc import App, InvokeMethodRequest, InvokeMethodResponse


# Enum of possible simulation status
class SimulationStatus(Enum):
    IDLE = 0
    RUNNING = 1
    STOPPED = 2
    FINISHED = 3


# Internal logic functions, for demo

def getFarmersData():
    farmersInformation = [
        {
            'id': 1,
            'sellIntention': True,
            'budget': '75000',
            'itemsToSell': [
                {
                    'id': 1,
                    'reservedPrice': 50000,
                    'size': 50.5,
                    'location': 'Seville',
                    'quality': 8
                },
                {
                    'id': 2,
                    'reservedPrice': 30000,
                    'size': 30,
                    'location': 'Huelva',
                    'quality': 6
                }
            ]
        },
        {
            'id': 2,
            'sellIntention': True,
            'budget': 50000,
            'itemsToSell': [
                {
                    'id': 3,
                    'reservedPrice': 20000,
                    'size': 20,
                    'location': 'Cordoba',
                    'quality': 5
                },
                {
                    'id': 4,
                    'reservedPrice': 40000,
                    'size': 43.5,
                    'location': 'Cadiz',
                    'quality': 7
                }
            ]
        }
    ]
    return farmersInformation


# Global variables

app = App()
status = SimulationStatus.RUNNING


# Internal logic functions, for demo

def updateLandData(auctionResults):
    # Placeholder, land ownership would be updated here for all agents
    return


# Message publication

def publishFarmersData(farmersData):
    with DaprClient() as d:
        resp = d.publish_event(
            pubsub_name='agricore_pubsub',
            topic_name='FARMERS_INFO_UPDATE',
            data=json.dumps(farmersData),
            data_content_type='application/json'
        )


# Message subscription

@app.subscribe(pubsub_name='agricore_pubsub', topic='LAND_AUCTION_RESULTS')
def processAuctionResults(event: v1.Event) -> None:
    auction_data = json.loads(event.Data())
    print(f'ABM received Auction results for', len(auction_data), 'lands that changed ownership. ', flush=True)
    updateLandData(auction_data)


# Create and expose invocable method

@app.method(name='getSimulationUpdate')
def getSimulationUpdate(request: InvokeMethodRequest) -> InvokeMethodResponse:
    print(request.metadata, flush=True)
    print(request.text(), flush=True)
    return InvokeMethodResponse(str(status), "text/plain; charset=UTF-8")


# Main app loop

print('ABM Starting')
# Initial wait for synchronization
sleep(10)
# Publish initial farmer's data
publishFarmersData(getFarmersData())
# Start listening for topics and invocations
app.run(50051)

