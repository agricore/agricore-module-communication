# General imports
from enum import Enum
import json
from time import sleep

from cloudevents.sdk.event import v1

# Dapr and gRPC related imports
from dapr.clients import DaprClient
from dapr.ext.grpc import App


# Enum of possible simulation status
class SimulationStatus(Enum):
    IDLE = 0
    RUNNING = 1
    STOPPED = 2
    FINISHED = 3


# Global variables

app = App()


# Internal logic functions, for demo

def reseolveAuctionProcess(farmersData):
    # Placeholder code, the logic for resolving auctions would be applied here
    auctionResult = [
        {
            'auctionId': 1,
            'itemId': 1,
            'sellerId': 1,
            'bidderId': 2,
            'amount': 50000
        },
        {
            'auctionId': 4,
            'itemId': 4,
            'sellerId': 2,
            'bidderId': 1,
            'amount': 40000
        }
    ]
    # Simulate processing time
    sleep(5)
    # Publish the results
    print('Auction resolved. Publishing data')
    publishAuctionResults(auctionResult)


# Message publication

def publishAuctionResults(auctionResults):
    with DaprClient() as d:
        resp = d.publish_event(
            pubsub_name='agricore_pubsub',
            topic_name='LAND_AUCTION_RESULTS',
            data=json.dumps(auctionResults),
            data_content_type='application/json'
        )


# Message subscription

@app.subscribe(pubsub_name='agricore_pubsub', topic='FARMERS_INFO_UPDATE')
def processAuctionResults(event: v1.Event) -> None:
    farmers_data = json.loads(event.Data())
    print(f'LMM received data  for', len(farmers_data), 'farmers. Starting auction process', flush=True)
    reseolveAuctionProcess(farmers_data)


# Main app loop

print('LMM Starting')
app.run(50052)

