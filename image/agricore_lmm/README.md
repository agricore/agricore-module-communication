# LMM - Land Market Module
Python app implementing AGRICORE's Land Market Module, which models the use and transfer of land resources among agents. In this module, agents will be able to exchange their land plot (parcel) objects, either permanently (buy-sell market) or temporarily (lease-rent market).

## License
© Open Source Licence - AGRICORE